def measure(num=1, &prc)
    now = Time.now
    num.times { prc.call }
    later = Time.now
    elapsed = later - now
    elapsed / num
end
