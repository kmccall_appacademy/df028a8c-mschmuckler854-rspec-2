def reverser(&p)
    string = p.call
    final = string.split(" ").map { |i| i.reverse }
    final.join(" ")
end

def adder(num=1)
    yield + num
end

def repeater(num=1, &p)
    num.times { p.call }
end
